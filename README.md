README
======
This is the README file for the PARSEME verbal multiword expressions (VMWEs) corpus for Turkish, edition 1.3. See the wiki pages of the [PARSEME corpora](https://gitlab.com/parseme/corpora/-/wikis/) initiative for the full documentation of the annotation principles.

The annotated Turkish 1.3 corpus is an updated and largely corrected version of the Turkish PARSEME 1.2 corpus. For the changes with respect to the 1.2 version see the change log.

The raw corpus released in edition 1.2 can be downloaded from a [dedicated page](https://gitlab.com/parseme/corpora/-/wikis/Raw-corpora-for-the-PARSEME-1.2-shared-task).

Annotated Corpus
--------------------

Turkish annotated corpus for the 1.3 edition comes from the Turkish newspaper corpus introduced in [1.1 edition](https://www.researchgate.net/publication/326276588_Turkish_verbal_multiword_expressions_corpus). 

The corpus consists of 
* 22,306 sentences, with the following VMWE counts: 
	* LVC.full: 3583 
	* VID: 4141
	* MVC: 5
The corpus has been re-parsed using UDPipe (as opposed to the ITU NLP Tool from edition 1.1, where the LEMMA column was problematic), relying on the model: turkish-imst-ud-2.4-190531.udpipe. After the dependency parser, previous annotations from edition 1.1 (which relied on the [PARSEME guidelines v 1.1]((http://parsemefr.lif.univ-mrs.fr/parseme-st-guidelines/1.1/) have been copied, and MWEs that have been missed, have been annotated.

Provided annotations
--------------------
The data are in the [.cupt](http://multiword.sourceforge.net/cupt-format) format. Here is detailed information about some columns:

* LEMMA (column 3): Available. Automatically annotated (UDPipe) and manually corrected in edition 1.3.
* UPOS and XPOS (column 4-5): Available. Automatically annotated (UDPipe) and manually corrected in edition 1.3.
* FEATS (column 6): Available. Automatically annotated (UDPipe) and manually corrected in edition 1.3.
* HEAD and DEPREL (columns 7 and 8): Available. Automatically annotated (UDPipe). Some annootations were manually corrected in edition 1.3.
* MISC (column 10): No-space information available. Automatically annotated (UDPipe).
* PARSEME:MWE (column 11): Manually annotated. The following [VMWE categories](http://parsemefr.lif.univ-mrs.fr/parseme-st-guidelines/1.1/?page=030_Categories_of_VMWEs) are annotated: LVC.full, VID, MVC.

Statistics
-------
To know the number of annotated VMWEs of different types and with different properties (length, continuity, etc.), use these scripts: [mwe-stats.py](https://gitlab.com/parseme/utilities/-/blob/master/st-organizers/corpus-statistics/mwe-stats.py) and [mwe-stats-simple.py](https://gitlab.com/parseme/utilities/-/blob/master/st-organizers/corpus-statistics/mwe-stats-simple.py). 

Authors
-------
Language Leader: Tunga Güngör (contact: gungort@boun.edu.tr). 
The annotation team consists of the following 10 members:
* Edition 1.3: Tunga Güngör, Büşra Marşan
* Edition 1.2: Tunga Güngör, Zeynep Yirmibeşoğlu
* Edition 1.1: Tunga Güngör, Berna Erden, Gözde Berk
* Edition 1.0: Gülşen Eryiğit, Kübra Adalı, Tutkum Dinç, Ayşenur Miral, Mert Boz


Contributors
------------
Some manual lemma and POS corrections for Edition 1.2: Yağmur Öztürk


Citation information
--------------------
Please refer to the following publication while using the Turkish annotated dataset:
@InProceedings{turkishdataset,
  author = {Berna Erden, Gozde Berk, and Tunga Gungor},
  title = {Turkish Verbal Multiword Expressions Corpus},
  booktitle = {26th IEEE Signal Processing and Communications Applications Conference, SIU 2018},
  month = {May},
  year = {2018},
  address = {İzmir, Turkey},
  pages={1-4}
  doi={10.1109/SIU.2018.8404583}
}

Licence
-------
The full dataset is licensed under Creative Commons Non-Commercial Share-Alike 4.0 licence [CC-BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode).


References
----------
@InProceedings{itunlp,
  author = {Eryigit, Gulsen},
  title = {ITU Turkish NLP Web Service},
  booktitle = {Proceedings of the Demonstrations at the 14th Conference of the European Chapter of the Association for Computational Linguistics (EACL)},
  month = {April},
  year = {2014},
  address = {Gothenburg, Sweden},
  publisher = {Association for Computational Linguistics},
}

@InProceedings{udturkish,
  author    = {Sulubacak, Umut  and  Gokirmak, Memduh  and  Tyers, Francis  and  Coltekin, Cagri  and  Nivre, Joakim  and  Eryigit, Gulsen},
  title     = {Universal Dependencies for Turkish},
  booktitle = {Proceedings of COLING 2016, the 26th International Conference on Computational Linguistics},
  month     = {December},
  year      = {2016},
  address   = {Osaka, Japan},
  publisher = {The COLING 2016 Organizing Committee},
  pages     = {3444--3454},
  url       = {http://aclweb.org/anthology/C16-1325}
}

@article{10.2307/41486039,
 ISSN = {1574020X, 15728412},
 URL = {http://www.jstor.org/stable/41486039},
 author = {Haşim Sak and Tunga Güngör and Murat Saraçlar},
 journal = {Language Resources and Evaluation},
 number = {2},
 pages = {249--261},
 publisher = {Springer},
 title = {Resources for Turkish morphological processing},
 volume = {45},
 year = {2011}
}

@inproceedings{ozturk-etal-2022-enhancing,
    title = "Enhancing the {PARSEME} {T}urkish Corpus of Verbal Multiword Expressions",
    author = "Ozturk, Yagmur  and
      Hadj Mohamed, Najet  and
      Lion-Bouton, Adam  and
      Savary, Agata",
    booktitle = "Proceedings of the 18th Workshop on Multiword Expressions @LREC2022",
    month = jun,
    year = "2022",
    address = "Marseille, France",
    publisher = "European Language Resources Association",
    url = "https://aclanthology.org/2022.mwe-1.14",
    pages = "100--104"
}

Change log
----------

- **2023-04-15**:
  - Version 1.3 of the corpus was released on LINDAT.
  - Changes with respect to the 1.2 version are the following:
    - Lemmas have been manually corrected
    - UPOS and XPOS tags have been manually corrected
    - Some head and dependency relation values have been manually corrected
    - Inconsistent annotations have been resolved
    - Missed verbal-MWEs have been annotated
- **2020-07-09**:
  - [Version 1.2](http://hdl.handle.net/11234/1-3367) of the corpus was released on LINDAT.
  - Changes with respect to the 1.1 version are the following:
    - The corpus has been re-parsed using UDPipe, thus completing the missing LEMMA tags in the previous version
    - Inconsistent annotations have been resolved
    - Missed verbal-MWEs have been annotated
    - A companion raw corpus, automatically annotated for morpho-syntax, was provided
- **2018-04-30**:
  - [Version 1.1](http://hdl.handle.net/11372/LRT-2842) of the corpus was released on LINDAT.
  - Changes with respect to the 1.0 version are the following:
    - updating the existing VMWE annotations to comply with PARSEME [guidelines edition 1.1](http://parsemefr.lif.univ-mrs.fr/parseme-st-guidelines/1.1/).
- **2017-01-20**:
  - [Version 1.0](http://hdl.handle.net/11372/LRT-2282) of the corpus was released on LINDAT.


