Total number of VMWEs: 367

### VMWE length (nb. of lexicalized components):
  * 2: 344 (93.73%)
  * 3: 22 (5.99%)
  * 4: 1 (0.27%)
  * ≥3: 23 (6.27%)
> Average length: 2.07
> Stddev length: 0.26

### VMWE dist (nb. of lexicalized components):
  * 1: 330 (89.92%)
  * 2: 26 (7.08%)
  * 3: 7 (1.91%)
  * 4: 2 (0.54%)
  * 5: 1 (0.27%)
  * 6: 1 (0.27%)
  * ≥3: 11 (3.00%)
> Average dist: 1.15
> Stddev dist: 0.53

### VMWE gaps (nb. of lexicalized components):
  * 0: 350 (95.37%)
  * 1: 10 (2.72%)
  * 2: 3 (0.82%)
  * 3: 2 (0.54%)
  * 4: 1 (0.27%)
  * 5: 1 (0.27%)
  * ≥3: 4 (1.09%)
> Average gaps: 0.08
> Stddev gaps: 0.46
