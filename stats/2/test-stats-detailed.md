Total number of VMWEs: 1151

### VMWE length (nb. of lexicalized components):
  * 1: 1 (0.09%)
  * 2: 1073 (93.22%)
  * 3: 70 (6.08%)
  * 4: 5 (0.43%)
  * 5: 2 (0.17%)
  * ≥3: 77 (6.69%)
> Average length: 2.07
> Stddev length: 0.30

### VMWE dist (nb. of lexicalized components):
  * 0: 1 (0.09%)
  * 1: 1030 (89.49%)
  * 2: 84 (7.30%)
  * 3: 22 (1.91%)
  * 4: 4 (0.35%)
  * 5: 4 (0.35%)
  * 6: 2 (0.17%)
  * 7: 1 (0.09%)
  * 8: 2 (0.17%)
  * 11: 1 (0.09%)
  * ≥3: 36 (3.13%)
> Average dist: 1.17
> Stddev dist: 0.67

### VMWE gaps (nb. of lexicalized components):
  * 0: 1094 (95.05%)
  * 1: 36 (3.13%)
  * 2: 10 (0.87%)
  * 3: 2 (0.17%)
  * 4: 5 (0.43%)
  * 5: 1 (0.09%)
  * 6: 1 (0.09%)
  * 7: 1 (0.09%)
  * 10: 1 (0.09%)
  * ≥3: 11 (0.96%)
> Average gaps: 0.10
> Stddev gaps: 0.57
