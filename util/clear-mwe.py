import argparse, os, re

THIS_DIR = os.path.dirname(os.path.realpath(__file__))

parser = argparse.ArgumentParser()
parser.add_argument('--folder', action="store", required=True)
args = parser.parse_args()

conllu_folderpath = args.folder
sentence_pattern = r'(.*?)\n\n'
tb = str()
for conllu_filepath in [i for i in os.listdir(conllu_folderpath) if i.endswith('.conllu')]:
    with open(conllu_filepath, 'r', encoding='utf-8') as f:
        tb += f.read() + '\n'
sentences = re.findall(sentence_pattern, tb, re.DOTALL)
new_tb = ''

for sentence in sentences:
    lines = sentence.split('\n')
    for j, line in enumerate(lines):
        fields = line.split('\t')
        if len(fields) == 11:
            line = '\t'.join(fields[:10])
        new_tb += f'{line}\n'
    new_tb += '\n'
with open(conllu_filepath, 'w', encoding='utf-8', newline='\n') as f:
    f.write(new_tb)
