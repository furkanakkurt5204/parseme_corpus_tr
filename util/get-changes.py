import argparse, os, re

THIS_DIR = os.path.dirname(os.path.realpath(__file__))

parser = argparse.ArgumentParser()
parser.add_argument('--prev', action="store", required=True)
parser.add_argument('--curr', action="store", required=True)
args = parser.parse_args()

sentence_pattern = r'(.*?)\n\n'

prev_folderpath = args.prev
prev_tb = str()
for prev_filepath in [i for i in os.listdir(prev_folderpath) if i.endswith('.conllu')]:
    with open(os.path.join(args.prev, prev_filepath), 'r', encoding='utf-8') as f:
        prev_tb += f.read()
prev_sents = re.findall(sentence_pattern, prev_tb, re.DOTALL)

curr_folderpath = args.curr
curr_tb = str()
for curr_filepath in [i for i in os.listdir(curr_folderpath) if i.endswith('.conllu')]:
    with open(os.path.join(args.curr, curr_filepath), 'r', encoding='utf-8') as f:
        curr_tb += f.read()
curr_sents = re.findall(sentence_pattern, curr_tb, re.DOTALL)

token_count = 0
feat_change_d = dict()
form_change_count = 0
lemma_change_count = 0
upos_change_count = 0
xpos_change_count = 0
for i in range(len(prev_sents)):
    prev_lines = prev_sents[i].split('\n')
    curr_lines = curr_sents[i].split('\n')
    for j in range(len(prev_lines)):
        prev_line = prev_lines[j]
        curr_line = curr_lines[j]
        prev_fields = prev_line.split('\t')
        curr_fields = curr_line.split('\t')
        if len(prev_fields) == 10:
            prev_id_t, prev_form_t, prev_lemma_t, prev_upos_t, prev_xpos_t, prev_feats_t = prev_fields[0:6]
            if prev_id_t.isnumeric():
                token_count += 1
            prev_feats_l = prev_feats_t.split('|')
            prev_feats_l = [feat for feat in prev_feats_l if feat != '_']
            prev_feat_d = dict()
            for feat in prev_feats_l:
                feat_name, feat_value = feat.split('=')
                prev_feat_d[feat_name] = feat_value

            curr_id_t, curr_form_t, curr_lemma_t, curr_upos_t, curr_xpos_t, curr_feats_t = curr_fields[0:6]
            curr_feats_l = curr_feats_t.split('|')
            curr_feats_l = [feat for feat in curr_feats_l if feat != '_']
            curr_feat_d = dict()
            for feat in curr_feats_l:
                feat_name, feat_value = feat.split('=')
                curr_feat_d[feat_name] = feat_value
            
            if prev_form_t != curr_form_t:
                form_change_count += 1
            if prev_lemma_t != curr_lemma_t:
                lemma_change_count += 1
            if prev_upos_t != curr_upos_t:
                upos_change_count += 1
            if prev_xpos_t != curr_xpos_t:
                xpos_change_count += 1
            feat_names = set(prev_feat_d.keys()).union(set(curr_feat_d.keys()))
            for feat_name in feat_names:
                if feat_name not in prev_feat_d.keys():
                    left_t = '_'
                    right_t = curr_feat_d[feat_name]
                elif feat_name not in curr_feat_d.keys():
                    left_t = prev_feat_d[feat_name]
                    right_t = '_'
                elif prev_feat_d[feat_name] != curr_feat_d[feat_name]:
                    left_t = prev_feat_d[feat_name]
                    right_t = curr_feat_d[feat_name]
                else:
                    continue
                if feat_name not in feat_change_d.keys():
                    feat_change_d[feat_name] = dict()
                if left_t not in feat_change_d[feat_name].keys():
                    feat_change_d[feat_name][left_t] = dict()
                if right_t not in feat_change_d[feat_name][left_t].keys():
                    feat_change_d[feat_name][left_t][right_t] = 0
                feat_change_d[feat_name][left_t][right_t] += 1

print('Sentence count:', len(prev_sents))
print('Token count:', token_count)
print('Form change count:', form_change_count)
print('Lemma change count:', lemma_change_count)
print('UPOS change count:', upos_change_count)
print('XPOS change count:', xpos_change_count)
print('Feature change counts:')
for feat_name in sorted(feat_change_d.keys()):
    print(feat_name)
    for left_t in sorted(feat_change_d[feat_name].keys()):
        for right_t in sorted(feat_change_d[feat_name][left_t].keys()):
            print('\t', left_t, '->', right_t, ':', feat_change_d[feat_name][left_t][right_t])